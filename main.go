// This is the entry point of our Go  program
package main

import (
	"fmt"
	"net/http"
)

func greetings(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello world")
}

func HandleRoutes() {
	http.HandleFunc("/", greetings)
	http.ListenAndServe(":8080", nil)
}

func main() {
	HandleRoutes()
}
